angular.module('directivaCalculadora',[])
    .directive('calculadora', function() {
  return {
    //require: '',
    restrict: 'E',
    //transclude: true,
    scope: {
      valor: '='
    },
    //link: function(scope, element, attrs, tabsCtrl) {
      //tabsCtrl.addPane(scope);
    //},
    templateUrl: 'templates/calculadora.html',
    controller: 'calculadoraController',
    controllerAs: 'calcCtrl'
  };
});