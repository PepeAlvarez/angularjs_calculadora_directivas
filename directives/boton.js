angular.module('directivaBoton', [])
  .directive('boton', function() {
    return {
      //require: '',
      restrict: 'E',
      replace: true,
      //transclude: true,
      scope: {
        accion: '&',
        valor: '='
        },
      //link: function(scope, element, attrs, tabsCtrl) {
        //tabsCtrl.addPane(scope);
      //},
      templateUrl: 'templates/boton.html'
    };
  });