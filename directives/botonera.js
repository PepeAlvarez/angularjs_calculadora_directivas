angular.module('directivaBotonera', [])
  .directive('botonera', function() {
    return {
      //require: '',
      restrict: 'E',
      //transclude: true,
      scope: {
        accion: '&',
        valor: '='
        },
      //link: function(scope, element, attrs, tabsCtrl) {
        //tabsCtrl.addPane(scope);
      //},
      templateUrl: 'templates/botonera.html'
    };
  });