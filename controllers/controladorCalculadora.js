angular.module('appCalculadora', ['directivaCalculadora','directivaDisplay','directivaBotonera','directivaBoton'])

  .controller('calculadoraController', function( $http) {

    this.display="";

    controlador = this;

    $http.get('json/datos.json')
       .then(function(resultado){
          controlador.filas = resultado.data;
        });

    this.insertaDato = function(dato){
      switch(dato)
      {
        case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
          this.display += dato;
          break;
        //case "×": case "÷":
        case "+": case "-": case "*": case "/":
          this.defineOperador(dato);
          break;
        case "=":
          this.operar();
          break;
        case "c":
          this.borrar();
      };
    };

    this.defineOperador = function (dato){
      this.operador = dato;
      this.operando1 = (this.display == "")?0:this.display;
      this.display = "";
    };

    this.operar = function(){
      this.operando2 = (this.display == "")?0:this.display;
      v1 = parseFloat(this.operando1);
      v2 = parseFloat(this.operando2);
      switch(this.operador){
        case "+":
          this.display = v1 + v2;
          break;
        case "-":
          this.display = v1 - v2;
          break;
        case "*":
          this.display = v1 * v2;
          break;
        case "/":
          this.display = v1 / v2;
          break;
        //case "×": case "÷":
      };
    };

    this.borrar = function(){
      this.display = "";
      this.operando1 = "";
      this.operando2 = "";
      this.operador = "";
    };
    
  })









.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})
;


